const { src, dest, watch} = require('gulp');
const sass = require('gulp-sass');
const minifyCSS = require('gulp-csso');

sass.compiler = require('node-sass');

const SCSS_ROOT = 'static/css/**/*.scss';

function scss() {
    return src(SCSS_ROOT)
        .pipe(sass())
        .pipe(minifyCSS())
        .pipe(dest(file => file.base))
}

exports.default = function() {
    watch(SCSS_ROOT, scss);
};
