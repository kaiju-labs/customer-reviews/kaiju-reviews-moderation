"""
Tests for abstract and base classes and interfaces.
"""

import pytest


@pytest.mark.unit
def test_nothing():
    """Just a stub."""
