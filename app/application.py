"""
Web app initialization.
"""

from pathlib import Path


from kaiju_tools.init_app import init_app as init_app_base

# DONT REMOVE
from app.service_registry import *
from .models import *
from .routes import urls

__all__ = ('init_app',)

DIR = Path(__file__).parent


def init_app(settings):
    app = init_app_base(settings, {'db_meta': metadata})
    app.db_meta = metadata

    router_settings = settings['etc']['routes']
    for method, reg, handler, name in urls:
        if reg in router_settings:
            reg = router_settings[reg]
        app.router.add_route(method, reg, handler)
        app.router.add_route(method, reg + "/", handler, name=name)
    return app
