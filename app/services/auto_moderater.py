from kaiju_tools.fixtures import ContextableService
from kaiju_tools.rpc.abc import AbstractRPCCompatible

from thefuzz import process

from .base_moderator import BaseAutoModerator, BaseAutoModeratorSettings


class AutoTextModerator(BaseAutoModerator):
    service_name = "AutoTextModerator"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, field='text', **kwargs)

    async def get_setting(self, setting_name):
        key_setting = f"{self.service_name}Settings-{setting_name}"
        data = await self.app.services.cache.get(key_setting)
        return data

    async def moderate_data(self, text):
        bad_words = await self.get_setting("bad_word")
        settings = await self.get_setting("number")
        result = process.extract(text, bad_words, limit=1)
        self.logger.warning(result)
        self.logger.warning(text)
        self.logger.warning(bad_words)
        if len(result) == 0:
            return True, [], "moderated", -1
        if result[0][1] > settings:
            return False, [], "no_moderated", -1
        else:
            return True, [], "moderated", -1


class AutoTextModeratorSettings(BaseAutoModeratorSettings, AbstractRPCCompatible):
    service_name = "AutoTextModeratorSettings"
    default_settings = {"number": 100,
                        "bad_word": []}

    @property
    def routes(self):
        return {
            **super().routes,
            'add_words': self.add_words,
            'test_moderate': self.test_moderate
        }

    @property
    def permissions(self):
        return {
            self.DEFAULT_PERMISSION: self.PermissionKeys.GLOBAL_GUEST_PERMISSION
        }

    async def test_moderate(self, text):
        key_bad_words = "auto_moderate_bad_words"
        bad_words = await self.app.services.cache.get(key_bad_words)
        key_settings = "auto_moderate_settings"
        settings = await self.app.services.cache.get(key_settings)
        result = process.extract(text, bad_words, limit=1)
        if len(result) == 0:
            return True
        if result[0][1] > settings:
            return False
        else:
            return True

    async def add_words(self, words):
        list_words = await self.get_setting("bad_words")
        list_words.extend(words)
        await self.set_setting("bad_words", list_words)
        return await self.get_setting("bad_word")

    async def remove_words(self, words):
        list_words = await self.get_setting("bad_words")
        set_old_words = set(list_words)
        set_remove_words = set(words)
        set_words = set_old_words - set_remove_words
        list_words = list(set_words)
        await self.set_setting("bad_words", list_words)
        return await self.get_setting("bad_word")
