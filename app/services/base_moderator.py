from kaiju_redis import RedisConsumer, RedisListener, RedisLocksService
from kaiju_tools.functions import retry
from kaiju_tools.encoding import serializers, MimeTypes
from kaiju_tools.fixtures import ContextableService
from kaiju_tools.rpc import AbstractRPCCompatible
from typing import Union, List, Tuple
from ..models.tables import settings_auto
from kaiju_tools.encoding.json import dumps, loads
from sqlalchemy.dialects.postgresql import insert


class ModeratorConsumer(RedisConsumer):

    def __init__(self, *args, class_moderator, **kwargs):
        self.class_moderators = []

        if isinstance(class_moderator, str):
            self.class_moderators_name = [class_moderator]
        if isinstance(class_moderator, list):
            self.class_moderators_name = class_moderator

        super().__init__(*args, **kwargs)

    async def init(self):
        await super().init()
        self.class_moderators = [self.discover_service(
            x, required=True) for x in self.class_moderators_name]

    async def _process_batch(self, batch):

        acks = []
        for topic, rows in batch.items():
            list_parameters = []
            for row in rows:
                row_id, data = row
                headers, body = data[b'h'], data[b'b']
                if body:
                    body = self._serializer.loads(body) if body else None
                    self.logger.info(body)
                    list_parameters.append(body)
                acks.append(row_id)
                print("_________________")
            if list_parameters:
                await self.modetates(list_parameters)
        if acks:
            await retry(
                self._transport.execute_command, args=(
                    'XACK', self.topic, self.group_id, *acks),
                retries=10, retry_timeout=1.0, logger=self.logger)

    async def modetates(self, list_parameters):
        list_reviews = await self.app.services.review.call(method="review.moderate_list_by_list",
                                                           params={"list_review": list_parameters})
        for review in list_reviews:
            results = []
            check = True
            for moderator in self.class_moderators:
                if moderator.field is None:
                    data_for_moderate = review['data']
                else:
                    data_for_moderate = review['data'][moderator.field]
                result = await moderator.moderate_data(data_for_moderate)
                results.append(result)
                check = check and result[0]
            await self.app.services.review.call(method="review.save_status_moderator", params={"review_id": review['review_id'],
                                                                                        "company_id": review['company_id'],
                                                                                        "moderators": results})
            if check:
                await self.app.services.review.call(method="review.all_moderator_ok", params={"review_id": review['review_id'],
                                                                                       "company_id": review['company_id']})


class ModeratorListener(RedisListener):
    consumer_class = ModeratorConsumer


class BaseAutoModerator(ContextableService):
    service_name = "BaseAutoModerator"

    def __init__(self, *args, field, keydb_service, review_service, **kwargs):
        super().__init__(*args, **kwargs)
        self.keydb_service_name = keydb_service
        self.review_service_name = review_service
        self.keydb_service = None
        self.review_service = None
        self.field = field
        self.key_all_moderators = "all-moderators-review-core"

    async def init(self):
        self.keydb_service = self.discover_service(
            self.keydb_service_name, required=True)
        self.review_service = self.discover_service(
            self.review_service_name, required=True)

    async def get_setting(self, setting_name):
        key_setting = f"{self.service_name}Settings-{setting_name}"
        data = await self.app.services.cache.get(key_setting)
        return data

    async def moderate_data(self, text) -> Tuple[bool, list, str, int]:
        pass


class BaseAutoModeratorSettings(ContextableService, AbstractRPCCompatible):
    service_name = "BaseAutoModeratorSettings"
    default_settings = {}

    @property
    def routes(self):
        return {
            'enable': self.enable,
            'disable': self.disable,
            'set_setting': self.set_setting,
            'get_setting': self.get_setting,
            'get_all_settings': self.get_all_setting
        }

    @property
    def permissions(self):
        return {
            self.DEFAULT_PERMISSION: self.PermissionKeys.GLOBAL_GUEST_PERMISSION
        }

    def __init__(self, *args, keydb_service, **kwargs):
        super().__init__(*args, **kwargs)
        self.keydb_service = None
        self.keydb_service_name = keydb_service
        self.key_all_moderators = "all-moderators-review-core"
        self.moderator_name = self.service_name.replace("Settings", '')

    async def init(self):
        await super().init()
        self.keydb_service = self.discover_service(
            self.keydb_service_name, required=True)
        statement_select = settings_auto.select().where(
            settings_auto.c.name_moderator == self.service_name)
        settings = {}
        async with self.app.services.db.acquire() as connection:
            settings = await connection.fetch(statement_select)
        if settings:
            if dict(settings[0]).get('enable', False):
                await self.keydb_service.sadd(self.key_all_moderators, self.service_name)

        else:
            insert = settings_auto.insert().values(name_moderator=self.service_name,
                                                   enable=False,
                                                   settings=self.default_settings).returning(
                settings_auto.c.name_moderator,
                settings_auto.c.enable,
                settings_auto.c.settings

            )
            async with self.app.services.db.acquire() as connection:
                settings = await connection.fetch(insert)
        settings = dict(settings[0]).get("settings", {})
        for settings_name, value in settings.items():
            key_setting = f"{self.service_name}-{settings_name}"
            data = await self.app.services.cache.get(key_setting)
            if data is None:
                await self.app.services.cache.set(key_setting, value)

    async def get_setting(self, setting_name):
        key_setting = f"{self.service_name}-{setting_name}"
        data = await self.app.services.cache.get(key_setting)
        if data is None:
            statement_select = settings_auto.select().where(
                settings_auto.c.name_moderator == self.service_name
            )
            async with self.app.services.db.acquire() as connection:
                data = await connection.fetch(statement_select)
            if data:
                return {
                    setting_name: dict(data[0])["settings"].get(
                        setting_name, None)
                }
        else:
            return {
                setting_name: data
            }
        return {}

    async def get_all_setting(self):
        key_setting = f"{self.service_name}-*"
        data = await self.app.services.cache.get(key_setting)
        if data is None:
            statement_select = settings_auto.select().where(
                settings_auto.c.name_moderator == self.service_name)
            async with self.app.services.db.acquire() as connection:
                settings = await connection.fetch(statement_select)
            if settings:
                return dict(settings[0])['settings']
        else:
            return data
        return data

    async def set_setting(self, setting_name, value):
        key_setting = f"{self.service_name}-{setting_name}"
        await self.app.services.cache.set(key_setting, value)
        async with self.app.services.db.acquire() as connection:
            result = await connection.fetch('''INSERT INTO
                                                         moderator(name_moderator,enable,settings)
                                                         VALUES ($1,$2,$3)
                                                         ON CONFLICT (name_moderator) DO UPDATE
                                                         SET settings = moderator.settings || excluded.settings
                                                         RETURNING name_moderator, enable, settings
                                                 ''', self.service_name, True, {setting_name: value})
            self.logger.info(result)
            return dict(result[0])

    async def enable(self):
        async with self.app.services.db.acquire() as connection:
            result = await connection.fetch('''INSERT INTO
                                                         moderator(name_moderator,enable,settings)
                                                         VALUES ($1,$2,$3)
                                                         ON CONFLICT (name_moderator) DO UPDATE
                                                         SET enable = excluded.enable
                                                         RETURNING name_moderator, enable, settings
                                                 ''', self.service_name, True, {})
            if dict(result[0]).get('enable'):
                await self.keydb_service.sadd(self.key_all_moderators, self.moderator_name)
            return dict(result[0])

    async def disable(self):
        async with self.app.services.db.acquire() as connection:
            result = await connection.fetch('''INSERT INTO
                                                         moderator(name_moderator,enable,settings)
                                                         VALUES ($1,$2,$3)
                                                         ON CONFLICT (name_moderator) DO UPDATE
                                                         SET enable = excluded.enable
                                                         RETURNING name_moderator, enable, settings
                                                 ''', self.service_name, False, {})
            if not dict(result[0]).get('enable'):
                await self.keydb_service.srem(self.key_all_moderators, self.moderator_name)
            return dict(result[0])
