from kaiju_tools.fixtures import ContextableService
from kaiju_tools.rpc.abc import AbstractRPCCompatible
from kaiju_tools.encoding.json import dumps, loads

from .base_moderator import BaseAutoModerator, BaseAutoModeratorSettings


class AutoCustomModerator(BaseAutoModerator):
    service_name = "AutoCustomModerator"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, field=None, **kwargs)

    @staticmethod
    def generate_rule(rules):
        list_rules = []

        for rule in rules:
            rules_field = {}
            if rule["enable"]:
                for field in rule.keys():
                    if field == "enable":
                        continue
                    if field == "option":
                        rules_field["option"] = rule["option"]
                        continue
                    if "contains" in rule[field] or "not_contains" in rule[field]:
                        for operator, word in rule[field].items():
                            if field not in rules_field:
                                rules_field[field] = {"type": "text"}
                            if operator not in rules_field[field]:
                                rules_field[field][operator] = []
                            rules_field[field][operator].extend(rule[field][operator])
                    if "begin" in rule[field] or "end" in rule[field]:
                        for operator, word in rule[field].items():
                            if field not in rules_field:
                                rules_field[field] = {"type": "text"}
                            rules_field[field][operator] = word
                    if "eq" in rule[field] or "lt" in rule[field] or "gt" in rule[field]:
                        if field not in rules_field:
                            rules_field[field] = {"type": "int"}
                        if "eq" in rule[field]:
                            rules_field[field]["eq"] = rule[field]["eq"]
                        if "lt" in rule[field]:
                            rules_field[field]["lt"] = min(rule[field]["lt"], rules_field[field].get("lt", 10000))
                        if "gt" in rule[field]:
                            rules_field[field]["gt"] = min(rule[field]["gt"], rules_field[field].get("gt", -10000))
                list_rules.append(rules_field)
        return list_rules

    @staticmethod
    def check_text_field(text, rule):
        list_response = []
        if "contains" in rule:
            for word in rule["contains"]:
                if text.find(word) == -1:
                    list_response.append(False)
        if "not_contains" in rule:
            for word in rule["not_contains"]:
                if text.find(word) != -1:
                    list_response.append(False)
        if "begin" in rule:
            if text[0:len(rule['begin'])] != rule['begin']:
                list_response.append(False)
        if "begin" in rule:
            if text[-len(rule['end']):] != rule['end']:
                list_response.append(False)
        list_response.append(True)
        return list_response

    @staticmethod
    def check_int_field(field, rule):
        list_response = []
        if isinstance(field, str):
            if field.isnumeric():
                field = int(field)
        if not isinstance(field, int):
            list_response.append(False)
            return list_response

        if "lt" in rule:
            if field >= rule["lt"]:
                list_response.append(False)
        if "gt" in rule:
            if field <= rule["gt"]:
                list_response.append(False)
        if "eq" in rule:
            if field != rule["eq"]:
                list_response.append(False)
        list_response.append(True)
        return list_response

    async def moderate_data(self, data):
        rules = await self.get_setting("rules")
        rules_field = self.generate_rule(rules)
        all_list_response_rule = []
        for rule in rules_field:
            list_response_rule = []
            option = rule.get("option", {"all_or_one": "all","tags":[],"status":"moderated", "priority":-1})
            for field in data.keys():
                if field in rule:
                    if rule[field]["type"] == "text":
                        list_response_rule.extend(self.check_text_field(data['field'], rule[field]))
                    if rule[field]["type"] == "int":
                        list_response_rule.extend(self.check_int_field(data['field'], rule[field]))
            result = True
            if option["all_or_one"] == "all":
                result = True
                for value in list_response_rule:
                    result &= value
            if option["all_or_one"] == "one":
                result = False
                for value in list_response_rule:
                    result |= value
            all_list_response_rule.append((result, option["tags"], option["status"],option["priority"]))
        result = True
        result_tags = []
        result_status = "no_moderated"
        result_priority = -1000
        for value, tags, status, priority in all_list_response_rule:
            result &= value
            if value:
                result_tags.extend(tags)
                if result_priority < priority:
                    result_status = status
                    result_priority = priority
        return result, result_tags, result_status, result_priority


class AutoTextModeratorSettings(BaseAutoModeratorSettings, AbstractRPCCompatible):
    service_name = "AutoCustomModeratorSettings"
    default_settings = {"rules": {},
                        "default_option": {
                            "action_after_success": "moderated"
                        }}

    @property
    def routes(self):
        return {
            **super().routes,
            'create_rule': self.create_rule,
            'set_default_option': self.set_default_option,
            'disable_rule': self.disable_rule,
            'enable_rule': self.enable_rule,
            'delete_rule': self.delete_rule,
            'edit_rule': self.create_rule,
            'list_rules': self.list_rules,
            'available_operator': self.operator
        }

    @property
    def permissions(self):
        return {
            self.DEFAULT_PERMISSION: self.PermissionKeys.GLOBAL_GUEST_PERMISSION
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    async def test_moderate(self, text):
        pass

    async def create_rule(self, name_rule: str, rule: dict):
        """
        name: str
        rule: dict
        Example rule
        "option":{
        "all_or_one":"all"||"one",
        "tags":[''],
        "priority": int,
        "action_after_success": "moderated"| "no_moderated"
        },
        "text_field":{
            "contains":[],
            "not_contains":[],
            "begin":"",
            "end":"",

        },
        "int_field":{
            "lt":10,
            "gt":0,
            "eq":5
        }
        }
        """
        rules = await self.get_setting("rules")
        if name_rule in rules:
            return False
        rule["enable"] = True
        rules[name_rule] = rule
        await self.set_setting("rules", rules)
        return True

    async def disable_rule(self, name_rule):
        rules = await self.get_setting("rules")
        if name_rule in rules:
            return False
        rules[name_rule]["enable"] = False
        await self.set_setting("rules", rules)
        return True

    async def enable_rule(self, name_rule):
        rules = await self.get_setting("rules")
        if name_rule in rules:
            return False
        rules[name_rule]["enable"] = True
        await self.set_setting("rules", rules)
        return True

    async def delete_rule(self, name_rule):
        rules = await self.get_setting("rules")
        if name_rule in rules:
            return False
        del rules[name_rule]
        await self.set_setting("rules", rules)
        return True

    async def list_rules(self):
        rules = await self.get_setting("rules")
        return rules
    async def change_priority_rules(self, list_priority_by_rule:dict):
        """
        {
           rules_name: priority
           ... 
        }
        """
        rules = await self.get_setting("rules")
        for name_rule, priority in list_priority_by_rule.items():
            if name_rule in rules:
                rules[name_rule]['priority'] = priority
        
            
    async def set_default_option(self, default_option):
        """
        "option":{
            "action_after_success": "tags"||"moderated",
            "tags":[""]
        }
        """
        await self.set_setting("default_option", default_option)
        return True
