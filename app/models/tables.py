"""
Write your table definitions here.
"""

from datetime import datetime

import sqlalchemy as sa
import sqlalchemy.dialects.postgresql as sa_pg

__all__ = (
    'metadata',
    'settings_auto',
    'custom_rules'
)

metadata = sa.MetaData()  #: user tables metadata

settings_auto = sa.Table("moderator", metadata,
                         sa.Column("name_moderator", sa.TEXT, primary_key=True),
                         sa.Column("enable", sa.BOOLEAN),
                         sa.Column("settings", sa_pg.JSONB, nullable=False, default={},
                                   server_default=sa.text("'{}'::jsonb")),
                         )

custom_rules = sa.Table("custom_rules", metadata,
                        sa.Column('rules_name', sa.TEXT, primary_key=True),
                        sa.Column('enable', sa.BOOLEAN),
                        sa.Column('rules', sa_pg.JSONB, nullable=False, default={},
                                  server_default=sa.text("'{}'::jsonb"))
                        )
